package models.interfaces;

import java.util.List;

/**
 * Created by Phil on 11.07.2016.
 */
public interface File {

    String getFilename();

    void setFilename(String filename);

    String getKategorie();

    void setKategorie(String kategorie);

    String getKommentar();

    void setKommentar(String kommentar);

    Course getCourse();

    void setCourse(Course course);

    void setPath(String path);

    String getPath();
}
