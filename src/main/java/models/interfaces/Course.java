package models.interfaces;

import java.util.List;

/**
 * Created by Phil on 11.07.2016.
 */
public interface Course {

    int getCourseId();

    void setCourseId(int courseId);

    String getName();

    void setName(String name);

    String getComment();

    void setComment(String comment);

    int getNote();

    void setNote(int note);

    String getSemester();

    void setSemester(String semester);

    List<File> getFiles();

    void setFiles(List<File> files);

    List<User> getUsers();

    void setUsers(List<User> users);

}
