package models.classes;

import models.interfaces.Course;
import models.interfaces.File;
import models.interfaces.User;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.util.List;

/**
 * Created by mikegoebel on 13.07.16.
 */
@Entity
@Table(name = "course", uniqueConstraints = @UniqueConstraint(columnNames = {"name"}))
public class CourseHibernateImpl implements Course {

    private int courseId;
    private String comment;
    private int note;
    private String name;
    private String semester;
    public static final String SOSE = "Sommersemester";
    public static final String WISE = "Wintersemester";
    private List<File> files;
    private List<User> users;

    @OneToMany(fetch = FetchType.EAGER, targetEntity = FileHibernateImpl.class, mappedBy = "course")
    public List<File> getFiles() {
        return files;
    }

    public void setFiles(List<File> files) {
        this.files = files;
    }


    public CourseHibernateImpl() {

    }

    /*

        public CourseHibernateImpl(int courseId, String comment, double note, String name) {
            this.courseId = courseId;
            this.comment = comment;
            this.note = note;
            this.name = name;
        }
    */
    public CourseHibernateImpl(String comment, int note, String name, String semester) {
        this.comment = comment;
        this.note = note;
        this.name = name;
        this.semester = semester;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public int getCourseId() {
        return courseId;
    }

    public void setCourseId(int courseId) {
        this.courseId = courseId;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getNote() {
        return note;
    }

    public void setNote(int note) {
        this.note = note;
    }

    // @ManyToOne(fetch = FetchType.EAGER,targetEntity = SemesterHibernateImpl.class)
    // @JoinColumn(name = "semesterId")
    public String getSemester() {
        return semester;
    }

    public void setSemester(String semester) {
        this.semester = semester;
    }

    @ManyToMany(fetch = FetchType.EAGER, mappedBy = "courseOnUsers", targetEntity = UserHibernateImpl.class)
    @Fetch(value = FetchMode.SUBSELECT)
    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

}
