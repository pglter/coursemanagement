package models.classes;

import models.interfaces.Course;
import models.interfaces.User;
import org.hibernate.annotations.Columns;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import java.util.List;

import static javax.swing.text.StyleConstants.Size;

/**
 * Created by mikegoebel on 13.07.16.
 */
@Entity
@Table(name = "user", uniqueConstraints = @UniqueConstraint(columnNames = {"username"}))
public class UserHibernateImpl implements User {

    private int userId;

    @NotEmpty
    @Column(unique = true)
    private String username;

    @NotEmpty
    private String name;
    @NotEmpty
    private String vorname;
    @NotEmpty
    private String matrikelnr;
    @NotEmpty
    private String email;
    @NotEmpty
    private String password;

    private List<Course> courseOnUsers;

    public UserHibernateImpl() {

    }

    public UserHibernateImpl(String username, String name, String vorname, String matrikelnr, String email, String password) {
        this.username = username;
        this.name = name;
        this.vorname = vorname;
        this.matrikelnr = matrikelnr;
        this.email = email;
        this.password = password;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getVorname() {
        return vorname;
    }

    public void setVorname(String vorname) {
        this.vorname = vorname;
    }

    public String getMatrikelnr() {
        return matrikelnr;
    }

    public void setMatrikelnr(String matrikelnr) {
        this.matrikelnr = matrikelnr;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @ManyToMany(targetEntity = CourseHibernateImpl.class)
    @JoinTable(name = "courseOnUsers", joinColumns = {@JoinColumn(name = "courseId_fk")}, inverseJoinColumns = {@JoinColumn(name = "userId_fk")})
    @LazyCollection(LazyCollectionOption.FALSE)
    public List<Course> getCourseOnUsers() {
        return courseOnUsers;
    }

    public void setCourseOnUsers(List<Course> courseOnUsers) {
        this.courseOnUsers = courseOnUsers;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public void setPassword(String password) {
        this.password = password;
    }
}
