package database;


import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.*;
import org.hibernate.cfg.Configuration;

/**
 * Created by mikegoebel on 21.07.16.
 */
public class SessionUtil {

    private static SessionFactory sessionfactory;

    public static SessionFactory getSessionFactory() {
        if (sessionfactory == null) {
            createSessionFactory();
        }

        return sessionfactory;
    }

    private static void createSessionFactory() {
        StandardServiceRegistry registry = new StandardServiceRegistryBuilder().configure().build();

        try {
            sessionfactory = new MetadataSources(registry).buildMetadata().buildSessionFactory();
            // sessionfactory = new Configuration().configure().buildSessionFactory();


        } catch (Exception e) {
            StandardServiceRegistryBuilder.destroy(registry);
            System.out.println("Create Session failed!!!");
        }
    }

    public static Session getSession() {
        return getSessionFactory().openSession();
    }

}