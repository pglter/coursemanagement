package database;

import controller.LoginController;
import models.classes.CourseHibernateImpl;
import models.interfaces.Course;
import models.interfaces.User;
import org.hibernate.Query;
import org.hibernate.Session;

import java.util.ArrayList;
import java.util.List;

import static controller.LoginController.getCurrentUserID;

/**
 * Created by mikegoebel on 18.07.16.
 */
public class courseDaoImpl {


    public courseDaoImpl() {
    }

    //TODO: Delete Methode anpassen
    public Course deleteCourse(Course course) {
        List<Course> courseonUser = LoginController.getCurrentUserID().getCourseOnUsers();
        for(int i = 0; i < courseonUser.size(); i++){
            if(courseonUser.get(i).getCourseId() == course.getCourseId()){
            courseonUser.remove(i);
            }
        }
        /*courseonUser.remove(course);
        for(int i = 0; i < courseonUser.size(); i++){
            System.out.println(courseonUser.get(i).getCourseId());
        }''''''*/
        Session s = SessionUtil.getSession();
        s.saveOrUpdate(LoginController.getCurrentUserID());
        //LoginController.getCurrentUserID().getCourseOnUsers().remove(course);
        s.beginTransaction();
        s.delete(course);
        s.getTransaction().commit();
        s.close();
        return course;
    }

    public Course createUpdateCourse(Course course) {
        Session s = SessionUtil.getSession();
        s.beginTransaction();
        s.saveOrUpdate(course);
        s.getTransaction().commit();
        s.close();
        return course;
    }

    /**
     * @param user
     * @return
     */
    public List<Course> getCoursesFromUser(User user) {
        Session s = SessionUtil.getSession();
        s.beginTransaction();
        // gibt vom uebergebenen User seine Courseliste zurück
        String queryString = "SELECT u FROM UserHibernateImpl u WHERE u.userId = :userId";
        Query query = s.createQuery(queryString);
        query.setInteger("userId", user.getUserId());
        User gefundenerUser = (User) query.uniqueResult();
        s.getTransaction().commit();
        s.close();
        return gefundenerUser.getCourseOnUsers();
    }


    /**
     * Returns all courses in registered in the database.
     *
     * @return
     */
    public List<Course> getCourses() {
        Session s = null;
        try {
            s = SessionUtil.getSession();
            s.beginTransaction();
            // Gibt alle Kurse zurück
            String queryString = "SELECT c FROM CourseHibernateImpl c";
            Query query = s.createQuery(queryString);
            List<Course> courseList = query.list();
            s.getTransaction().commit();

            return courseList;
        } catch (Exception ex) {
            s.getTransaction().rollback();
        } finally {
            s.close();
        }
        return null;
    }

    public Course getCoursebyName(String cName) {
        Session s = SessionUtil.getSession();
        s.beginTransaction();
        String queryString = "SELECT c FROM CourseHibernateImpl c WHERE c.name = :name";
        Query query = s.createQuery(queryString);
        query.setParameter("name", cName);
        Course course = (Course) query.uniqueResult();
        s.getTransaction().commit();
        s.close();

        return course;
    }

    /**
     * Returns course with specified ID from the database
     *
     * @param id
     * @return Course
     * @Philipp Gölter
     * @date 01.09.2016
     */
    public Course getCourseById(int id) {
        Session s = null;
        try {
            s = SessionUtil.getSession();
            s.beginTransaction();
            Course course = (Course) s.get(CourseHibernateImpl.class, id);
            s.getTransaction().commit();
            return course;
        } catch (Exception ex) {
            s.getTransaction().rollback();
        } finally {
            s.close();
        }
        return null;
    }

    public Course subscribeToCourse(Course course, User user) {
        // user einschreiben
        user.getCourseOnUsers().add(course);
        Session s = SessionUtil.getSession();
        s.beginTransaction();
        s.saveOrUpdate(user);
        s.saveOrUpdate(course);
        s.getTransaction().commit();
        s.close();
        return course;
    }

}
