package database;

import javafx.collections.ObservableList;
import models.classes.UserHibernateImpl;
import models.interfaces.User;
import org.hibernate.Query;
import org.hibernate.Session;

import java.util.List;

/**
 * Created by mikegoebel on 18.07.16.
 */

public class userDaoImpl {

    public userDaoImpl() {
    }


    public User delete(User user) {
        Session s = SessionUtil.getSession();
        s.beginTransaction();
        s.saveOrUpdate(user);
        s.getTransaction().commit();
        s.close();
        return user;
    }

    public User getUserById(int id) {
        Session s = SessionUtil.getSession();
        s.beginTransaction();
        User user = (User) s.get(UserHibernateImpl.class, id);
        s.getTransaction().commit();
        s.close();
        return user;
    }

    public User createUpdateUser(User user) {
        try {
            Session s = SessionUtil.getSession();
            s.beginTransaction();
            s.saveOrUpdate(user);
            s.getTransaction().commit();
            s.close();
            return user;
        } catch (java.lang.ExceptionInInitializerError e) {
            return null;
        } catch (java.lang.NoClassDefFoundError e) {
            return null;
        } catch (javax.persistence.PersistenceException e) {
            return null;
        }
    }

    public User getUserbyName(String name) {
        Session s = SessionUtil.getSession();
        s.beginTransaction();
        String queryString = "SELECT u FROM UserHibernateImpl u WHERE u.username = :username";
        Query query = s.createQuery(queryString);
        query.setParameter("username", name);
        User gefundenerUser = (User) query.uniqueResult();
        s.getTransaction().commit();
        s.close();

        return gefundenerUser;
    }

    /**
     * Von Olbertz erstellt asl Beispiel! Bei den andern Methoden try{}catch{}finally{} ergänzen
     *
     * @return
     */
    public List<User> getUsers() {
        Session s = null;
        try {
            s = SessionUtil.getSession();
            s.beginTransaction();
            // gibt vom uebergebenen User seine Courseliste zurück
            String queryString = "SELECT u FROM UserHibernateImpl u";
            Query query = s.createQuery(queryString);
            List<User> userList = query.list();
            s.getTransaction().commit();

            return userList;
        } catch (Exception ex) {
            s.getTransaction().rollback();
        } finally {
            s.close();
        }
        return null;
    }

}
