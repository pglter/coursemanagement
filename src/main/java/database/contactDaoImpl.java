package database;

import models.classes.ContactHibernateImpl;
import models.interfaces.Contact;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.mapping.Array;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mikegoebel on 18.07.16.
 */
public class contactDaoImpl {

    public contactDaoImpl() {

    }

    public Contact createUpdateContact(Contact contact) {
        Session s = SessionUtil.getSession();
        s.beginTransaction();
        s.saveOrUpdate(contact);
        s.getTransaction().commit();
        s.close();
        return contact;
    }

    public Contact deleteContact(Contact contact) {
        Session s = SessionUtil.getSession();
        s.beginTransaction();
        s.delete(contact);
        s.getTransaction().commit();
        s.close();
        return contact;
    }

    public ArrayList<ContactHibernateImpl> getContacts() {
        Session s = null;
        try {
            s = SessionUtil.getSession();
            s.beginTransaction();
            // Gibt alle Contacts zurück
            String queryString = "SELECT c FROM ContactHibernateImpl c";
            Query query = s.createQuery(queryString);
            List<ContactHibernateImpl> contactList = query.list();
            s.getTransaction().commit();
            ArrayList<ContactHibernateImpl> contactsListenew  = new ArrayList<ContactHibernateImpl>(contactList);

            return contactsListenew;
        } catch (Exception ex) {
            s.getTransaction().rollback();
        } finally {
            s.close();
        }
        return null;
    }

}
