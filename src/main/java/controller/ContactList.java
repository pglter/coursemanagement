package controller;

import database.contactDaoImpl;
import models.classes.ContactHibernateImpl;
import models.interfaces.Contact;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 *  Diese Klasse ist zum verwalten der Email-Kontaktliste
 */
public class ContactList {
    private ArrayList<ContactHibernateImpl> contacts = new ArrayList<ContactHibernateImpl>();
    contactDaoImpl impl = new contactDaoImpl();
    ContactHibernateImpl con = new ContactHibernateImpl();

    /**
     * Sucht einen Kontakt per eingegebener Email-Adresse
     * @param emailAddress
     * @return place
     */
    public int whereIsContact(String emailAddress) {
        int place = -1;
        if (!contacts.isEmpty()) {
            place = Collections.binarySearch(contacts, new ContactHibernateImpl("", "", emailAddress));
        }
        return place;
    }

    /**
     * Gibt die Liste der Kontakte zurück
     * @return contacts
     */
    public List<ContactHibernateImpl> getContacts()
    {
         contacts = impl.getContacts();

        return contacts;
    }

    /**
     * Fügt einen Kontakt mit Vornam, Name und Email hinzu. Existiert dieser Kontakt schon wird eine Fehlermeldung angezeigt.
     * @param contact
     * @return true or false
     */
    public boolean addContact(Contact contact) {
        if (whereIsContact(contact.getEmail()) < 0) {
            this.con = new ContactHibernateImpl(contact.getVorname(),contact.getName(),contact.getEmail());
            impl.createUpdateContact(this.con);
            Collections.sort(contacts);
            System.out.println("Der Kontakt mit dieser E-Mail-Addresse wurde hinzugefügt: " + contact.getEmail());
            return true;
        } else {
            System.out.println("Der Kontakt mit dieser E-Mail-Addresse gibt es schon: " + contact.getEmail());
            return false;
        }
    }

    /**
     * Löscht den eingegebenen Konzakt.
     * @param contact
     * @return true or false
     */
    public boolean removeContact(Contact contact) {
        int place = whereIsContact(contact.getEmail());
        if (place >= 0) {
            System.out.println("Der Kontakt mit dieser E-Mail-Addresse wird gelöscht: " + contact.getEmail());
            contacts.remove(place);
            this.con = new ContactHibernateImpl(contact.getVorname(),contact.getName(),contact.getEmail());
            impl.deleteContact(con);
            return true;
        } else {
            System.out.println("Der Kontakt konnte nicht gelöscht werden.");
            return false;
        }
    }

    /**
     * Daten des Kontaktes werden verändert.
     * @param i
     * @param contact
     * @return true or false
     */
    public boolean editContact(int i, Contact contact) {
        int place = whereIsContact(contact.getEmail());
        if (place < 0 || place == i) {
            con = new ContactHibernateImpl(contact.getVorname(), contact.getName(), contact.getEmail());
            contacts.add(con);
            impl.deleteContact(contacts.get(i));
            contacts.remove(i);
            impl.createUpdateContact(contact);
            Collections.sort(contacts);
            System.out.println("Der Kontakt wurde geändert.");
            return true;
        } else {
            System.out.println("Der Kontakt mit dieser E-Mail-Addresse gibt es schon: " + contact.getEmail());
            return false;
        }
    }
}
