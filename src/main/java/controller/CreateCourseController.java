package controller;

import com.jfoenix.controls.*;
import com.sun.org.apache.xpath.internal.operations.Number;
import core.CourseManagement;
import core.StaticBag;
import database.courseDaoImpl;
import database.userDaoImpl;
import gui.Toast;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleGroup;
import javafx.stage.Stage;
import models.classes.CourseHibernateImpl;
import models.interfaces.Course;


/**
 * Diese Kontrol Klasse erstellt unsere Kurse in unserer Overview Scene.
 */
public class CreateCourseController {
    @FXML
    private JFXRadioButton soSe;
    @FXML
    private JFXRadioButton wiSe;
    @FXML
    private JFXTextField kursBezeichnung;
    @FXML
    private JFXButton createKursBtn;
    @FXML
    private JFXTextArea kursKommentar;
    @FXML
    private JFXTextField note;
    @FXML
    private String semesterString;

    private StructureManager struct = new StructureManager();
    private boolean kursBez = false;
    private boolean semester = false;
    private courseDaoImpl courseDao = new courseDaoImpl();

    /**
     * Erlaubt zugriff auf den zuletzt erstellten Kurs
     * @return lastCreatedCourse
     * @author Philipp Gölter
     */
    public static Course getLastCreatedCourse() {
        return lastCreatedCourse;
    }

    private static Course lastCreatedCourse;

    private static boolean lastCourseCreated;
    public CreateCourseController() {
    }

    /**
     * Wird bei dem Start automatisch initialisiert
     */
    @FXML
    private void initialize() {
        initCreateKursTab();

    }

    /**
     * Zeigt den zuletzt erstellten Kurs an
     * @return lastCourse Created
     */
    public static boolean isLastCourseCreated() {
        return lastCourseCreated;
    }

    public static void setLastCourseCreated(boolean lastCourseCreated) {
        CreateCourseController.lastCourseCreated = lastCourseCreated;
    }

    /**
     * Wird zum erstellen des Kurses benötigt
     */
    private void initCreateKursTab() {

        ToggleGroup semesterBtns = new ToggleGroup();

        soSe.setToggleGroup(semesterBtns);
        wiSe.setToggleGroup(semesterBtns);
        createKursBtn.setDisable(true);

        /**
         * Kontrolliert die Bedingungen eines Kursnamens
         */
        kursBezeichnung.textProperty().addListener((observable, oldValue, newValue) -> {
            semester = wiSe.isSelected() || soSe.isSelected();
            String trimString = kursBezeichnung.getText().trim();
            if (trimString.matches("^( |\\-|\\/|\\(|\\)|\\w|&|ü|#|\\+|ö|ä|Ä|Ü|Ö|ß)+$")) {
                kursBez = true;

            } else {
                kursBez = false;
                createKursBtn.setDisable(true);
            }
            compareSemKurs(kursBez, semester);
        });


        createKursBtn.setOnAction((ActionEvent event) -> {
            if (note.getText().matches("(?:\\d*\\.)?\\d+"))
                lastCreatedCourse = createCourse();
            else
                Toast.makeErrorMessage(((Stage) createKursBtn.getScene().getWindow()), "Bitte Zahl eingeben!", 3500, 200, 200);


        });

        semesterBtns.selectedToggleProperty().addListener(new ChangeListener<Toggle>() {
            @Override
            public void changed(ObservableValue<? extends Toggle> observable, Toggle oldValue, Toggle newValue) {
                semester = wiSe.isSelected() || soSe.isSelected();
                compareSemKurs(kursBez, semester);
            }
        });
    }

    /**
     * Erstellt den Kurs und speichert in der Datenbank ab
     * @return course
     */
    private Course createCourse() {
        Course course = new CourseHibernateImpl();
        if (soSe.isSelected()) {
            semesterString = CourseHibernateImpl.SOSE;
            course.setSemester(CourseHibernateImpl.SOSE);
            struct.createCourseIfNotExist(LoginController.getCurrentUserID().getName(), semesterString, kursBezeichnung.getText());
        } else if (wiSe.isSelected()) {
            semesterString = CourseHibernateImpl.WISE;
            course.setSemester(CourseHibernateImpl.WISE);
            struct.createCourseIfNotExist(LoginController.getCurrentUserID().getName(), semesterString, kursBezeichnung.getText());
        }


        course.setName(kursBezeichnung.getText());
        course.setNote(convertStringToInt(note.getText()));


        course.setComment(kursKommentar.getText());

        courseDao.createUpdateCourse(course);
        courseDao.subscribeToCourse(course, LoginController.getCurrentUserID());

        // Bei jedem neu erstellten Kurs muss der Observable List das neue Element hinzugefügt werden
        StaticBag.addCourseToList(semesterString, course.getName());
        ((Stage) createKursBtn.getScene().getWindow()).close();
        return course;
    }

    /**
     * Solange kein Name für den Semesterkurs eingetragen ist bleibt der creatKursBtn ohne Funktion
     * @param kursBez
     * @param semester
     */
    public void compareSemKurs(boolean kursBez, boolean semester) {
        if (kursBez && semester) {
            createKursBtn.setDisable(false);
        }
    }

    /**
     * Konvertiert beim erstellen des Kurses den eingegebennen String bei ETCS in ein Int um
     * @param note
     * @return noteInt
     */
    private int convertStringToInt(String note) {
        note.trim();
        int noteInt = 0;
        noteInt = Integer.parseInt(note);
        return noteInt;

    }
}
