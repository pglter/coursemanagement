package controller;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * In dieser Klasse werden jeweilige Ordner, Kurse und Dateien von Benutzern bearbeitet.
 */
public class StructureManager {

    /**
     * Erstellt den root-Ordner Kursunterlagenverwaltung
     */
    public void initialisationStructure() {
        createFolder("Kursunterlagenverwaltung");
        createFolder("Kursunterlagenverwaltung/Users");
        createFolder("Kursunterlagenverwaltung/Styles");
    }

    /**
     * Erstellt den Benutzer und Semester-Ordner
     *
     * @param userName Name des Benutzers
     * @throws IOException Gibt eine Fehlermeldung zurück wenn der Ordner nicht erstellt wurde
     */
    public void createUserIfNotExist(String userName) {
        initialisationStructure();
        String overFolder = "Kursunterlagenverwaltung/Users/" + userName;
        String[] folders = new String[]{overFolder, overFolder + "/Sommersemester", overFolder + "/Wintersemester"};
        createFolders(folders);
    }

    /**
     * Erstellt den Kurs-Ordner sowie deren Kategorien
     *
     * @param userName Name des Benutzers
     * @param semester Semester des Kurses
     * @param course   Name des Kurses
     */
    public void createCourseIfNotExist(String userName, String semester, String course) {
        initialisationStructure();
        createUserIfNotExist(userName);
        String overFolder = "Kursunterlagenverwaltung/Users/" + userName + "/" + semester + "/" + course;
        String[] folders = new String[]{overFolder, overFolder + "/Skripte", overFolder + "/Uebungen"};
        createFolders(folders);
    }

    /**
     * Löscht den Kurs eines Benutzers
     *
     * @param userName Name des Benutzers
     * @param semester Semester des Kurses
     * @param course   Name des Kurses
     */
    public void removeCourse(String userName, String semester, String course) {
        String overFolder = "Kursunterlagenverwaltung/Users/" + userName + "/" + semester + "/" + course;
        File coursePath = new File(overFolder);
        removeFolder(coursePath);
    }

    /**
     * Löscht einen Ordner seine Unterverzeichnisse und Dateien
     *
     * @param directory Pfad des Ordners
     */
    public void removeFolder(File directory) {
        if (directory.exists()) {
            File[] files = directory.listFiles();
            if (files != null) {
                for (int i = 0; i < files.length; i++) {
                    if (files[i].isDirectory()) {
                        removeFolder(files[i]);
                    } else {
                        removeFile(files[i].getPath());
                    }
                }
                try {
                    directory.delete();
                    System.out.println("Ordner wurde gelöscht: " + directory.getPath());
                } catch (SecurityException e) {
                    System.err.println("Ordner wurde nicht gelöscht: " + directory.getPath() + e.getMessage());
                }
            }
        }
    }

    /**
     * Löscht eine Datei
     *
     * @param filePath Pfad der Datei
     */
    public void removeFile(String filePath) {
        if (checkIfExist(filePath)) {
            File file = new File(filePath);
            try {
                file.delete();
                System.out.println("Datei wurde gelöscht: " + filePath);
            } catch (SecurityException e) {
                System.err.println("Datei wurde nicht gelöscht: " + filePath + e.getMessage());
            }
        }
    }

    /**
     * Gibt aus ob der Ordner existiert oder nicht
     *
     * @param folderPath Verzeichnis des Ordners
     * @return true, der Ordner existiert, false der existiert nicht
     */
    public boolean checkIfExist(String folderPath) {
        Path path = Paths.get(folderPath);
        return Files.exists(path);
    }

    /**
     * Erstellt einen Ordner wenn es nicht existiert
     *
     * @param folderPath Verzeichnis des Ordners
     */
    public void createFolder(String folderPath) {
        if (!checkIfExist(folderPath)) {
            File dir = new File(folderPath);
            try {
                dir.mkdirs();
                System.out.println("Ordner erstellt: " + folderPath);
            } catch (SecurityException e) {
                System.err.println("Ordner kann nicht erstellt werden: " + folderPath + e.getMessage());
            }
        }
    }

    /**
     * Erstellt mehrere Ordner wenn die nicht existieren
     *
     * @param foldersPath Verzeichnisse der Ordner
     */
    public void createFolders(String[] foldersPath) {
        for (String folderPath : foldersPath) {
            createFolder(folderPath);
        }
    }

    /**
     * Bennent eine Datei/einen Ordner um
     *
     * @param pathString    Aktueller Pfad
     * @param newPathString Neuer Pfad
     */
    public void rename(String pathString, String newPathString) {
        File filePath = new File(pathString);
        File newFilePath = new File(newPathString);
        createFolder(newFilePath.getPath());
        try {
            filePath.renameTo(newFilePath);
            System.out.println("Wurde umbenannt auf: " + newPathString);
        } catch (SecurityException e) {
            System.err.println("Kann nicht umbenannt werden auf: " + pathString + e.getMessage());
        } catch (NullPointerException e) {
            System.err.println("Kann nicht umbenannt werden auf: " + pathString + e.getMessage());
        }
    }

    /**
     * Kopiert eine Datei in einem Ordner
     *
     * @param source Quelle der Datei
     * @param dest   Ziel der Datei
     */
    public void copyFile(File source, File dest) {
        try {
            Files.copy(source.toPath(), dest.toPath());
            System.out.println("Datei wurde kopiert auf: " + dest.getPath());
        } catch (IOException e) {
            System.err.println("Datei kann nicht kopiert werden: " + source + e.getMessage());
        }
    }

    /**
     * Kopier einen Upload im richtigen Ordner des Benutzers
     *
     * @param userName Benutzername
     * @param semester Semesters des Kurses
     * @param course   Kursname
     * @param category Dateikategorie
     * @param source   Pfad der Datei die upgeloaded wird
     */
    public String copyUpload(String userName, String semester, String course, String category, String source) {
        File fileSource = new File(source);
        ComboBoxFilePathObject fileName = new ComboBoxFilePathObject(source);
        File fileDest = new File("Kursunterlagenverwaltung/Users/" + userName + "/" + semester + "/" + course + "/" + category + "/" + fileName.toString());
        copyFile(fileSource, fileDest);
        return fileDest.getAbsolutePath();
    }
}

