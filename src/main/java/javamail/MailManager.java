package javamail;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Properties;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.activation.DataHandler;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;

import controller.ComboBoxFilePathObject;

/**
 * Diese Klasse wird zum eigentlichen versenden der Email verwendet
 */
public class MailManager {
    private String emailOfSender;
    private String passwordOfSender;
    private String zipName;
    private String subject;
    private String emailText;
    private String installationPath;

    /**
     * Ausgabe der Liste der Dateien
     *
     * @return Liste der Datei
     */
    public ArrayList<ComboBoxFilePathObject> getListOfFiles() {
        return listOfFiles;
    }

    private ArrayList<ComboBoxFilePathObject> listOfFiles = new ArrayList<>();
    private ArrayList<String> listOfContacts = new ArrayList<>();

    /**
     * Setzen der E-Mail-Adresse des Senders
     *
     * @param emailOfSender E-Mail-Adresse des Senders
     */
    public void setEmailOfSender(String emailOfSender) {
        this.emailOfSender = emailOfSender;
    }

    /**
     * Ausgabe der E-Mail-Adresse des Senders
     *
     * @return E-Mail-Adresse des Senders
     */
    public String getEmailOfSender() {
        return emailOfSender;
    }

    /**
     * Ausgabe des Namen vom zip
     *
     * @return Zip-Name
     */
    public String getZipName() {
        return zipName;
    }

    /**
     * Ausgabe des Betreffs
     *
     * @return Betreffname
     */
    public String getSubject() {
        return subject;
    }

    /**
     * Ausgabe der E-Mail-Nachricht
     *
     * @return E-Mail-Nachricht
     */
    public String getEmailText() {
        return emailText;
    }

    /**
     * Ausgabe des Installatiounspfad
     *
     * @return Installatiounspfad
     */
    public String getInstallationPath() {
        return installationPath;
    }

    /**
     * Setzen des Senderpasswords
     *
     * @param passwordOfSender Senderpasswords
     */
    public void setPasswordOfSender(String passwordOfSender) {
        this.passwordOfSender = passwordOfSender;
    }

    /**
     * Setzen des Zip-Namen
     *
     * @param zipName Zip-Namen
     */
    public void setZipName(String zipName) {
        this.zipName = zipName;
    }

    /**
     * Setzen des Betreffnamen
     *
     * @param subject Betreffnamen
     */
    public void setSubject(String subject) {
        this.subject = subject;
    }

    /**
     * Setzen der E-Mail-Nachricht
     *
     * @param emailText E-Mail-Nachricht
     */
    public void setEmailText(String emailText) {
        this.emailText = emailText;
    }

    /**
     * Setzen des Installationspfad
     *
     * @param installationPath Installationspfad
     */
    public void setInstallationPath(String installationPath) {
        this.installationPath = installationPath;
    }

    /**
     * Neuen Empfänger hinzufügen
     *
     * @param contact Empfänger
     */
    public void addNewContact(String contact) {
        listOfContacts.add(contact);
    }

    /**
     * Neue Datei hinzufügen
     *
     * @param file Datei
     */
    public void addNewFile(String file) {
        boolean add = true;
        for (ComboBoxFilePathObject pathFile : listOfFiles) {
            if (pathFile.getFilePath().compareTo(file) == 0) {
                add = false;
            }
        }
        if (add) {
            listOfFiles.add(new ComboBoxFilePathObject(file));
            System.out.println("Anhang hinzugefügt: " + file);
        }
    }

    /**
     * Alle Dateien löschen
     */
    public void deleteFileSelection(String filePathToDelete) {
        for (int i = 0; i < listOfFiles.size(); i++) {
            if (listOfFiles.get(i).getFilePath().compareTo(filePathToDelete) == 0) {
                listOfFiles.remove(i);
                System.out.println("Anhang gelöscht: " + filePathToDelete);
            }
        }
        if (listOfFiles.size() == 0) {
            zipName = null;
        }
    }

    /**
     * Alle Empfänger als ArrayList ausgeben
     *
     * @return ArrayList von Empfängern
     */
    public ArrayList showContacts() {
        return listOfContacts;
    }

    /**
     * Sendet eine E-Mail mit Empfänger, Betreff, Dateien verpackt in einem zip
     */
    public void sendMail() {
        ByteArrayOutputStream baos = null;

        // Kontrolliert ob überhaupt ein Anhang gesendet wird
        if (zipName != null) {
            baos = zipSelection(listOfFiles, installationPath);
        }

        // SMTP-Server funktioniert für den Moment nur mit gmail
        String host = "smtp.gmail.com";

        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", host);
        props.put("mail.smtp.port", "587");

        // Timeout
        props.put("mail.smtp.connectiontimeout", "9000"); // 9 Sekunden
        props.put("mail.smtp.timeout", "9000"); // 9 Sekunden

        // Ausgabe des Session-Objekts
        Session session = Session.getInstance(props,
                new javax.mail.Authenticator() {
                    @Override
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(emailOfSender, passwordOfSender);
                    }
                });
        try {
            // Einen Standard Mime-Message-Objekt erstellen
            Message message = new MimeMessage(session);

            // Setzt das Header-Feld
            message.setFrom(new InternetAddress(emailOfSender));

            String emailAddresses = "";
            for (int i = 0; i < listOfContacts.size() - 1; i++) {
                emailAddresses += listOfContacts.get(i) + ",";
            }
            emailAddresses += listOfContacts.get(listOfContacts.size() - 1);

            message.addRecipients(Message.RecipientType.TO, InternetAddress.parse(emailAddresses));

            // Betreff setzen
            if (subject == null || subject.compareTo("") == 0) {
                subject = "Kein Betreff";
            }
            message.setSubject(subject);

            // Nachrichten-Teil erstellen
            BodyPart messageBodyPart = new MimeBodyPart();

            // Nachricht setzen
            if (emailText == null) {
                emailText = "";
            }
            messageBodyPart.setText(emailText);

            // Eine Nachhricht aus mehreren Teile erstellen
            Multipart multipart = new MimeMultipart();

            // Nachrichten-Teil setzen
            multipart.addBodyPart(messageBodyPart);

            // Anhang
            if (zipName != null) {
                messageBodyPart = new MimeBodyPart();

                ByteArrayDataSource ds = new ByteArrayDataSource(baos.toByteArray(), "application/zip");
                messageBodyPart.setDataHandler(new DataHandler(ds));
                messageBodyPart.setFileName(zipName);

                multipart.addBodyPart(messageBodyPart);
            }

            // Nachhrichten-Teile alle senden
            message.setContent(multipart);

            // Nachhricht wird gesendet
            Transport.send(message);
            System.out.println("Die E-Mail wurde versendet");

        } catch (MessagingException e) {
            System.err.println("E-Mail konnte nicht gesendet werden: " + e.getMessage());
        }
    }

    /**
     * Erstellt im virtuellen Speicher eine Zip
     *
     * @param listOfFiles      Liste der zu sendenden Dateien
     * @param installationPath Pfad des Programms
     * @return virtueller Zip
     */
    public ByteArrayOutputStream zipSelection(ArrayList<ComboBoxFilePathObject> listOfFiles, String installationPath) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try (ZipOutputStream zos = new ZipOutputStream(baos)) {
            for (ComboBoxFilePathObject filePath : listOfFiles) {
                ZipEntry entry = null;
                if (installationPath.compareTo("") != 0) {
                    entry = new ZipEntry(filePath.toString().replaceFirst(installationPath, ""));
                } else {
                    entry = new ZipEntry(filePath.toString());
                }
                Path path = Paths.get(filePath.getFilePath());
                byte[] data = Files.readAllBytes(path);
                zos.putNextEntry(entry);
                zos.write(data);
            }
            zos.closeEntry();
            System.out.println("ZIP ist erstellt");
        } catch (IOException e) {
            System.err.println("ZIP konnte nicht erstellt werden: " + e.getMessage());
        }
        return baos;
    }
}
