package core;

import controller.LoginController;
import controller.StructureManager;
import database.SessionUtil;
import database.userDaoImpl;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Group;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Screen;
import javafx.stage.Stage;
import models.classes.UserHibernateImpl;
import org.hibernate.SessionFactory;

import java.io.IOException;

/**
 * Diese Klasse ist für das Laden der Klasen zuständig
 */
public class CourseManagement extends Application {

    Stage primaryStage;
    Stage dialogStage;

    public boolean showNewDialog(String titel, String fxmlpath) throws IOException {
        try {
            CourseManagement cm = new CourseManagement();
            // Lädt das fxml file und erstellt eine neue stage für popup dialoge
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(CourseManagement.class.getResource(fxmlpath));
            AnchorPane page = (AnchorPane) loader.load();

            Stage localstage = new Stage();
            // Erstellt die Dialog Stage

            localstage.setTitle(titel);
            localstage.initModality(Modality.WINDOW_MODAL);
            localstage.initOwner(primaryStage);
            Scene scene = new Scene(page);
            localstage.setScene(scene);

            localstage.show();

            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        StructureManager struct = new StructureManager();
        this.primaryStage = primaryStage;
        Parent root = FXMLLoader.load(getClass().getResource("../scenes/Login.fxml"));
        this.primaryStage.setTitle("Kursverwaltung-HTW");
        this.primaryStage.setResizable(false);
        this.primaryStage.setMinHeight(392);
        this.primaryStage.setMinWidth(532);
        this.primaryStage.setScene(new Scene(root));
        this.primaryStage.show();
       /* String css = this.getClass().getResource("Default_Theme_1.css").toExternalForm();
        LoginController lo = new LoginController();
                lo.login.getScene().getStylesheets().add(css);*/
    }

    public static void main(String[] args) {
        SessionUtil.getSessionFactory();
        userDaoImpl dao = new userDaoImpl();
        launch(args);
        SessionUtil.getSessionFactory().close();

    }
}
